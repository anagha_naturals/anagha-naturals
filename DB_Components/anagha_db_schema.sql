-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2018 at 06:52 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `anagha_db_schema`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `string1` varchar(255) NOT NULL,
  `string2` varchar(255) NOT NULL,
  `place` varchar(255) NOT NULL,
  `landmark` varchar(255) NOT NULL,
  `pincode` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address`
--

INSERT INTO `address` (`id`, `userid`, `string1`, `string2`, `place`, `landmark`, `pincode`) VALUES
(1, 1, '139, AB Apratments', 'CD Road, Annai Nagar', 'Perungudi', 'Near AB Bakery', 600098),
(3, 2, '139, AB Apratments', 'CD Road, Annai Nagar', 'Perungudi', 'Near AB Bakery', 600096),
(5, 3, '139, AB Apratments', 'CD Road, Annai Nagar', 'Perungudi', 'Near AB Bakery', 600101),
(20, 4, 'test', 'address', 'chennai', 'sp infocity', 600056),
(22, 20, 'test', 'user', 'sp ', 'infocity', 12);

--
-- Triggers `address`
--
DELIMITER $$
CREATE TRIGGER `empty_check_address_on_insert` BEFORE INSERT ON `address` FOR EACH ROW BEGIN
IF NEW.string1 = '' THEN
	SET NEW.string1 = NULL;
END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `empty_check_address_on_update` BEFORE UPDATE ON `address` FOR EACH ROW BEGIN
IF NEW.string1 = '' THEN
	SET NEW.string1 = NULL; 
END IF;
IF NEW.string2 = '' THEN
	SET NEW.string2 = NULL; 
END IF;
IF NEW.place = '' THEN
	SET NEW.place = NULL; 
END IF;
IF NEW.landmark = '' THEN
	SET NEW.landmark = NULL; 
END IF;
IF NEW.pincode = '' THEN
	SET NEW.pincode = NULL; 
END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `statusid` int(11) NOT NULL,
  `netcost` int(11) NOT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `userid`, `statusid`, `netcost`, `created`) VALUES
(89, 2, 1, 250, '2018-11-12'),
(90, 2, 3, 250, '2018-11-12'),
(91, 2, 2, 530, '2018-11-12'),
(93, 2, 1, 1000, '2018-11-21');

-- --------------------------------------------------------

--
-- Table structure for table `orderitem`
--

CREATE TABLE `orderitem` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `total_amount_per_product` int(255) NOT NULL,
  `orderid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderitem`
--

INSERT INTO `orderitem` (`id`, `product_name`, `quantity`, `total_amount_per_product`, `orderid`) VALUES
(69, 'Groundnut Oil', 1, 250, 89),
(70, 'Groundnut Oil', 1, 250, 90),
(71, 'Groundnut Oil', 1, 250, 91),
(74, 'Organic Idly Rice(25Kg)', 1, 1000, 93);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(255) NOT NULL,
  `description` text NOT NULL,
  `category` varchar(255) NOT NULL,
  `image` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `description`, `category`, `image`) VALUES
(2, 'Butter(0.5Kg)', 200, '', 'Dairy product', ''),
(1, 'Coconut Oil', 300, 'Oil which is made by Coconuts', 'Oil', ''),
(3, 'Groundnut Oil', 250, 'Oil made from Groundnuts', 'Oil', ''),
(6, 'Organic Idly Rice(25Kg)', 1000, '', 'Grains', ''),
(5, 'Organic Ponni Rice(25Kg)', 1750, '', 'grains', ''),
(4, 'Sesame Oil', 300, 'Oil made from Sesame seeds.', 'Oil', '');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `notificationmessage` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `name`, `notificationmessage`) VALUES
(1, 'NEW', 'Your order is placed successfully'),
(2, 'PROCESSING', 'Your order is received and currently in progress'),
(3, 'DELIVERED', 'Your order is delivered successfully');

-- --------------------------------------------------------

--
-- Table structure for table `userprofile`
--

CREATE TABLE `userprofile` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `age` tinyint(3) NOT NULL,
  `contactnumber` bigint(13) NOT NULL,
  `role` varchar(20) NOT NULL,
  `created` date NOT NULL,
  `deleted` date NOT NULL,
  `session_id` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table for User profile Basic information';

--
-- Dumping data for table `userprofile`
--

INSERT INTO `userprofile` (`id`, `firstname`, `lastname`, `email`, `password`, `age`, `contactnumber`, `role`, `created`, `deleted`, `session_id`) VALUES
(1, 'Monika', 'V', 'vmonika@gmail.com', 'dm1vbmlrYQ==', 23, 1234567890, 'admin', '2018-06-10', '0000-00-00', 'qwertyuiop1234567890'),
(2, 'Ranjitha', 'L', 'loganranji@gmail.com', 'bG9nYW5yYW5qaQ==', 23, 1234567890, 'admin', '2018-06-10', '0000-00-00', 'qeu9dobhvsvtrp340rn9rr6di8'),
(3, 'Delip', 'S', 'pshenbagadelip@gmail.com', 'ZGVsaXA=', 23, 1234567892, 'user', '2018-06-10', '0000-00-00', 'qwertyuiop1234567892'),
(4, 'test', 'testers', 'test@gmail.com', 'MTIzNDU=', 0, 999, 'admin', '2018-09-11', '0000-00-00', 'il5oudaeqinev3avufssfc8v42');

--
-- Triggers `userprofile`
--
DELIMITER $$
CREATE TRIGGER `empty_check_userprofile` BEFORE UPDATE ON `userprofile` FOR EACH ROW BEGIN
IF NEW.firstname = '' THEN
	SET NEW.firstname = NULL; 
END IF;
IF NEW.lastname = '' THEN
	SET NEW.lastname = NULL;
END IF;
IF NEW.contactnumber = '' THEN
	SET NEW.contactnumber = NULL;
END IF;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderitem`
--
ALTER TABLE `orderitem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `productid` (`id`);

--
-- Indexes for table `userprofile`
--
ALTER TABLE `userprofile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `orderitem`
--
ALTER TABLE `orderitem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `userprofile`
--
ALTER TABLE `userprofile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

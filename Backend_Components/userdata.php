<?php
  
require_once 'util.php';
$con = connectSQL();
$session_id = checkClientData("SID");

//Update userprofile table with filled in details.
if(isset($_GET['userdata'])) {
  $data = json_decode($_GET['userdata'],true);
  $firstname = $data['firstName'];
  $lastname = $data['lastName'];
  if($data['age'] != '' ){
    $age = $data['age'];
  }else{
    $age='NULL';
  }
  $string1 = $data['address1'];
  $string2 = $data['address2'];
  $place = $data['place'];
  $landmark = $data['landmark'];
  $pincode = $data['pincode']; 
  $mobilenumber = $data['mobilenumber']; 
  // Get the userid
  $userid=$_SESSION["userid"];

  //Update the userprofile table with passed in values.
  $update_userprofile_query = "UPDATE userprofile set age=$age,firstname='$firstname' ,lastname='$lastname' ,contactnumber=$mobilenumber  where id=$userid";
  $update_userprofile_result = $con->query($update_userprofile_query);

  //End the function if the update is not successful.
  if(!$update_userprofile_result){
    echo "failure";
    return;
  }

  //Fetch address id for the userprofile being updated.
  $check_address_query = "SELECT id from address where userid=$userid";
  $check_address_result = $con->query($check_address_query);

  //Insert a new entry to address table, if no related address is found.
  if(mysqli_num_rows($check_address_result)==0){
    $update_address_query="INSERT INTO `address` (`id`, `userid`, `string1`, `string2`, `place`, `landmark`,`pincode`) VALUES (NULL,'$userid', '$string1', '$string2' , '$place','$landmark','$pincode')";
    $update_address_result= $con->query($update_address_query);
    //End the function if the update is not successful.
    if(!$update_address_result){
      echo "failure";
      return;
    }
  }

  //Update the values in address table.
  else{
      $update_address_query = "UPDATE address set string1='$string1', string2='$string2', place='$place', landmark='$landmark', pincode='$pincode' where userid=$userid";
      $update_address_result= $con->query($update_address_query);
      //End the function if the update is not successful.
      if(!$update_address_result){
        echo "failure";
        return;
      }
    }
}

//Function to display user values in user profile tab.
if(isset($_GET['displayuserdata'])) {
  $userid=$_SESSION["userid"];

  //Fetch the userdata from userprofile and address tables.
  $get_userdata_query = "SELECT `userprofile`.`firstname`,`userprofile`.`lastname`,`userprofile`.`age`,`userprofile`.`contactnumber`,`address`.`string1`,`address`.`string2`,`address`.`place`,`address`.`landmark`,`address`.`pincode` from `userprofile` LEFT OUTER JOIN `address` on `userprofile`.`id` = `address`.`userid` where `userprofile`.`id`=$userid";
  $get_userdata_result = $con->query($get_userdata_query);
  $user_result = array();
  while($row= $get_userdata_result->fetch_assoc()){
    array_push($user_result, $row);
  }
  $result = json_encode($user_result);
  echo $result;
}

//Get the user access for the given user id.
if(isset($_GET['userid'])) {
  $userid = $_SESSION["userid"];
  $get_userrole_query = "select `userprofile`.`id` from `userprofile` where `userprofile`.`role`=\"admin\" and `userprofile`.`id`=$userid";
  $get_userrole_result = $con->query($get_userrole_query);
  if($get_userrole_result->num_rows == 0){
    echo false;
  }
  else{
    echo true;
  }
}

if(isset($_GET['email'])){
  $email = $_GET["email"];
  $check_user_name = $con->query("select id from userprofile where email = \"$email\"");
  if($check_user_name->num_rows == 0){
    echo false;
  }
  else{
    echo true;
  }
}

if(isset($_GET['mail']) && isset($_GET['password'])){
  $email = $_GET['mail'];
  $password = base64_encode($_GET['password']);
  $check_entry = $con->query("select id from userprofile where email=\"$email\" and password=\"$password\"");
  if($check_entry->num_rows == 0){
    echo "false";
  }
  else{
    echo "true";
  }
}

//Function to fetch username for the session user.
if(isset($_GET['getusername'])){
  $user_id=$_SESSION["userid"];
  $get_user_name = $con->query("select firstname from userprofile where id=$user_id");
  $user_name = $get_user_name->fetch_assoc();
  $user_name=json_encode($user_name);
  echo $user_name;
  return $user_name;
}

?>
<?php

//Util file
require 'util.php';
session_start();

//************DB Connection************
$con = connectSQL();

//**********Admin Related Actions*******************
//******** Get the List of Orders for all Users depending on Status
if (isset($_GET['get_all_orders_by_status'])) {
	$get_status_id = $_GET['get_all_orders_by_status'];
	$get_all_orders_query="select `order`.`id`, CONCAT(`address`.`string1`,',',`address`.`string2`,',',`address`.`place`,',',`address`.`pincode`,'.') address ,`order`.`created`,`userprofile`.`firstname` from `order` INNER JOIN `userprofile` ON `order`.`userid`=`userprofile`.`id` INNER JOIN `address` ON `userprofile`.`id` = `address`.`userid` where `order`.`statusid`=$get_status_id order by `order`.`created` desc";
	$get_all_orders_result = $con->query($get_all_orders_query);
	$order_array=array();
	while ($each_order = $get_all_orders_result->fetch_assoc()){
    array_push($order_array, $each_order);
	}
	$result=json_encode($order_array);
	echo $result;
	return $result;
}

//Function to get status id and name
if(isset($_GET['get_status'])){
	$get_status_data_query = "select id,name from status";
	$get_status_data = $con->query($get_status_data_query);
	$result_array = array();
	while ($each_status = $get_status_data->fetch_assoc()){
		$status=array();
		$status['id'] = $each_status['id'];
		$status['name'] = $each_status['name'];
		array_push($result_array, $status);
	}
	$status_result = json_encode($result_array);
	echo $status_result;
}

//Function to get item list based on status.
if(isset($_GET['get_items_by_status'])){
	$statusid = $_GET['get_items_by_status'];
	$get_items_query = "select `orderitem`.`product_name`,count(`orderitem`.`id`) quantity from `orderitem` INNER JOIN `order` ON `orderitem`.`orderid`= `order`.`id` WHERE `order`.`statusid` = $statusid group by product_name";
	$get_items_data = $con->query($get_items_query);
	$result_array = array();
	while( $each_item = $get_items_data->fetch_assoc()){
		$items = array();
		$items['name'] = $each_item['product_name'];
		$items['qty'] = $each_item['quantity'];
		array_push($result_array, $items);
	}
	$items_result = json_encode($result_array);
	echo $items_result;
}

//Get the status id given the name.
if(isset($_GET['get_status_id_by_value'])) {

	$status_name = $_GET['get_status_id_by_value'];
	$get_status_query = "select `status`.`id` from status where `status`.`name` =\"$status_name\"";
	$get_status_result = $con->query($get_status_query);

	while($row= $get_status_result->fetch_assoc()){
		$result = $row['id'];
	}
	echo $result;
	return $result;
}

//*** For a given OrderId get the detailed OrderItem Details
if(isset($_GET['get_orderitem_details']) ){
	$orderid = $_GET['get_orderitem_details'];
	// Get the Order Item details
	$get_orderitem_details_query="select `product_name`,`quantity` from `orderitem` where `orderid`= $orderid ";
	$get_orderitem_details_result = $con->query($get_orderitem_details_query);
	$orderitem_array = array();
	while($each_orderitem = $get_orderitem_details_result->fetch_assoc()){
		array_push($orderitem_array,$each_orderitem);
	}
	$result=json_encode($orderitem_array);
	echo $result;
	return $result;

}

//****Update the Status of an Order with the given status. Ex: Prcoessing - 2; Delivered -3
if(isset($_GET['statusid']) && isset($_GET['orderid'])){
	$get_status_id = $_GET['statusid'];
	$get_order_id = $_GET['orderid'];
	echo $get_status_id;
	//echo "Inside";
	//Update the Order Table with the required status
	$update_orderitem_query = "UPDATE `order` set `statusid`= $get_status_id WHERE `id`= $get_order_id";
	if($con->query($update_orderitem_query) == true){
		return true;
		echo "True";
	}
	else{
		return false;
		echo "False";
	}
}

?>
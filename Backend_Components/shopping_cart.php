<?php

//Util file
require 'util.php';
session_start();

//************DB Connection details************
$con = connectSQL();
// $userid = $_SESSION['userid'];
$userid = $_SESSION["userid"];
$products = array(); // Indexed Arrays
$cost_of_products = array(); //Associative arrays
$product_id = array();
$total_cart_value =0; // Initial total cart value
date_default_timezone_set('Asia/Kolkata');
$current_date =  date("Y-m-d");


//Get the list of all products,cost from DB
$query_product_price="select id, name, price from product";
$x= $con->query($query_product_price);  

while($row = $x->fetch_assoc()){
	array_push($products, $row['name']);
	$product_id[$row['name']] = $row['id'];
	$cost_of_products[$row['name']] = $row['price'];
} 

// Function to list all products 
if( isset($_GET['get_all_products_with_price'])){
	$get_all_products_with_price = $con->query("select name, price from product");
	$all_products = array();
	while($each_row = $get_all_products_with_price->fetch_assoc()){
	$each_row["qty"]=0;
	if( isset($_SESSION["cart"])){
		if(in_array($each_row["name"], $_SESSION["cart"])){
			$each_row["qty"]=$_SESSION["qty"][$each_row["name"]];
		}
	}
		array_push($all_products,$each_row);
	}
	$all_products_json=json_encode($all_products);
	echo $all_products_json;
	return $all_products_json; 
}

//Function to get session variables on to UI.
if( isset($_GET['getcart']) && isset($_GET['cartvalue'])) {

	unset($_SESSION["qty"]); //The quantity for each product
	unset($_SESSION["total_amount_per_product"]); //The amount from each product
	unset($_SESSION["total_cart_value"]); //The total cost
	unset($_SESSION["cart"]); //Which item has been chosen

	//if( isset($_GET['getcart'])) {
	$data = json_decode($_GET["cartvalue"],true);
	foreach ($data as $key => $value) {
		$_SESSION["qty"][$key] = $value;
		$_SESSION["total_amount_per_product"][$key] = $cost_of_products[$key]*$value;
		$_SESSION["cart"][$key] = $key;
	}

	$newar=array();
	$_SESSION["total_cart_value"]=0;
	if(isset($_SESSION["cart"])){
		foreach($_SESSION["cart"] as $value){
			$ar=array();
			$ar["name"]=$value;
			$ar["quantity"]=$_SESSION["qty"][$value];
			$ar["cost"]=$cost_of_products[$value];
			$ar["total_amount_per_product"]=$_SESSION["total_amount_per_product"][$value];
			$_SESSION["total_cart_value"] = $_SESSION["total_cart_value"]+$_SESSION["total_amount_per_product"][$value]; 
			array_push($newar,$ar);
		}
	}
	else{
		return null;
	}
	$result=json_encode($newar);
	echo $result;
}

if( isset($_GET['getquantity'])){
	$newarray=array();
	foreach($_SESSION["cart"] as $value){
		$newarray[$value]=$_SESSION["qty"][$value];
	}
	$result=json_encode($newarray);
	echo $result;
}

//Function to get user address

if(isset($_GET['getuseraddress'])) {
	$userid = $_SESSION['userid'];
	$get_address_query = "select `address`.`string1`,`address`.`string2`,`address`.`place`,`address`.`landmark`,`address`.`pincode` from `address` INNER JOIN `userprofile` on `address`.`userid` = `userprofile`.`id` where `userprofile`.`id` = $userid";
	$get_address_result = $con->query($get_address_query);
	$newarray=array();
	while ($row = $get_address_result->fetch_assoc()){
    	array_push($newarray, $row);
	}
	$result=json_encode($newarray);
	echo $result;
}


//Load up session variable arrays to handle cart for each product
if ( !isset($_SESSION["total_cart_value"]) ) {
	$_SESSION["total_cart_value"] = 0;
	for ($i=0; $i< count($products); $i++) {
		$_SESSION["qty"][$products[$i]] = 0;
		$_SESSION["total_amount_per_product"][$products[$i]] = 0;
	}
}
//---------------------------
//Reset cart
if ( isset($_GET['reset']) && $_GET["reset"] == 'true' ) {
		unset($_SESSION["qty"]); //The quantity for each product
		unset($_SESSION["total_amount_per_product"]); //The amount from each product
		unset($_SESSION["total_cart_value"]); //The total cost
		unset($_SESSION["cart"]); //Which item has been chosen
	}

//---------------------------
//Add Items to Cart
	if ( isset($_GET["add"]) ) {
		$i = $_GET["add"];
		$qty = $_SESSION["qty"][$i] + 1;
		$_SESSION["total_amount_per_product"][$i] = $cost_of_products[$i] * $qty;
		$_SESSION["cart"][$i] = $i;
		$_SESSION["qty"][$i] = $qty;
		if($_SESSION["cart"]!= null ){
			return true;
		}
		else {
		    return false;
		}
	}
//---------------------------

//Delete an Item from Cart
	if ( isset($_GET["delete"]) ) {
		$i = $_GET["delete"];
		$_SESSION["qty"][$i]--;
		$qty =  $_SESSION["qty"][$i];
		//remove item if quantity is zero
		if ($qty == 0) {
			$_SESSION["total_amount_per_product"][$i] = 0;
			unset($_SESSION["cart"][$i]);
		}
		else{
			$_SESSION["total_amount_per_product"][$i] = $cost_of_products[$i] * $qty;
		}
	}
//Push the Items into Order and OrderItem table - Checkout
	if ( isset($_GET["checkout"]) && isset($_SESSION["cart"]) ) {  

	// Get the Total Cart Amount
		foreach ( $_SESSION["cart"] as $i ) {
			$total_cart_value = $total_cart_value + $_SESSION["total_amount_per_product"][$i];
		}

	//Push the values to DB once the item is checkout
		$update_order_query="INSERT INTO `order` (`id`, `userid`, `statusid`, `netcost`, `created`) VALUES (NULL, '$userid', 1 , $total_cart_value, '$current_date')";
		$update_order_result= $con->query($update_order_query);
		if($update_order_result) {
			$get_orderid_result= $con->query("SELECT max(`id`) as `orderid` from `order` where `userid`='$userid'");
		// Get the latest Order id of the Current User
			while($row = $get_orderid_result->fetch_assoc()){
				$orderid=$row['orderid'];
			}

	 // Proceed to update in OrderItem if update to Order table is Successful
			foreach ( $_SESSION["cart"] as $i ) {	
				$each_qty = $_SESSION["qty"][$i];
				$total_amount_per_product = $_SESSION["total_amount_per_product"][$i];	
	//Push the items into OrderItem table
				$update_orderitem_query="INSERT INTO orderitem (`id`, `product_name`, `quantity`, `total_amount_per_product`,`orderid`) VALUES 
				(NULL, '$i', $each_qty , $total_amount_per_product , $orderid)";
				$update_orderitem_result = $con->query($update_orderitem_query);
			//echo $update_orderitem_result->connect_error;		
			}
		}
		return $update_order_result;
	}
	?>
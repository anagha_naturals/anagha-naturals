<?php 
require_once 'util.php';
$conn = connectSQL();
require 'PHPMailer/src/PHPMailer.php'; 
require 'PHPMailer/src/SMTP.php';
require 'PHPMailer/src/Exception.php';

if(isset($_GET['email'])) {
    $email = $_GET['email'];
    $recoveryQuery = "SELECT PASSWORD,firstname FROM userprofile WHERE email LIKE '$email'";
    $result = $conn->query($recoveryQuery);
    $row = $result->fetch_assoc();
    $username= $row['firstname'];
    $password= $row['PASSWORD'];

    $link = "<a href='http://localhost/anagha/anagha/update_password.html'> password recovery </a> ";

    date_default_timezone_set('America/New_York');
    $mail = new PHPMailer\PHPMailer\PHPMailer();
    // Optionally get email body from external file
    $body = "Hi ".$username.", <br> <br> We have sent you this email in response to your request to recover your password. Your password is <b>".base64_decode($password)."</b>. <br><br>We recommend that you keep your password secure and not share it with anyone. You can change your password anytime by clicking on the \"Reset Password\" link in the mobile application. <br><br> Sincerely,<br>Anagha Naturals Customer Service ";
    $mail->IsSMTP();                            // telling the class to use SMTP
    $mail->Host       = "smtp.gmail.com";       // SMTP server
    //$mail->SMTPDebug  = 4;                      // enables SMTP debug information (for testing)
                                                    // 0 default no debugging messages
                                                    // 1 = errors and messages
                                                    // 2 = messages only
    $mail->SMTPAuth   = true;                   // enable SMTP authentication
    //$mail->SMTPSecure = 'ssl';                // Not supported
    $mail->SMTPSecure = 'tls';                  // Supported
    $mail->Host       = "smtp.gmail.com";       // sets the SMTP server
    $mail->Port       = 587;                    // set the SMTP port for the GMAIL server
    $mail->Username   = "anagha.stayorganic@gmail.com";         // SMTP account username (how you login at gmail)
    $mail->Password   = "Stayorganic@1";      // SMTP account password (how you login at gmail)
 
    $mail->setFrom('anagha.stayorganic@gmail.com', 'Anagha-Naturals');
 
    $mail->addReplyTo('anagha.stayorganic@gmail.com', 'Anagha-Naturals');
 
    $mail->Subject    = "Anagha-Naturals password recovery ";
 
    $mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and 
 
    $mail->msgHTML($body);
    $mail->AddAddress($email);
 
    if(!$mail->Send()) {
      echo false;
      return false;
    } else {
      echo true;
      return true;
    }
}
?>
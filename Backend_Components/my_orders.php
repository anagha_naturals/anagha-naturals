<?php

//Util file
require 'util.php';
session_start();

//************DB Connection************
$con = connectSQL();

//Declaration of product's price, which is later used in the function below.
$get_all_products_with_price = $con->query("select name, price from product");
$product_price = array();
while($each_row = $get_all_products_with_price->fetch_assoc()){
	$product_price[$each_row['name']]=$each_row['price'];
}


//Function to show the product name, quantity and price for each item in my orders.
if( isset($_GET['orderid'])){
	$orderid = $_GET['orderid'];
	$get_order_item_query = $con->query("select product_name, quantity from orderitem where orderid = $orderid");
	$order_item_array = array();
	while($each_row = $get_order_item_query->fetch_assoc()){
		$orderitem = array();
		$orderitem["name"] = $each_row["product_name"];
		$orderitem["quantity"] = $each_row["quantity"];
		$orderitem["price"] = $product_price[$each_row["product_name"]];
		array_push($order_item_array, $orderitem);
	}
	$result=json_encode($order_item_array);
	echo $result;
}

//Function to get all the orders associated with an user sorted by status and order placed date.
if( isset($_GET['userid'])){
	$userid=$_SESSION["userid"];
	$get_order_query = $con->query("select `order`.`id`,`status`.`name`,`order`.`netcost`,`order`.`created` from `order` INNER JOIN `status` ON `order`.`statusid` = `status`.`id` where `order`.`userid`=$userid order by `order`.`statusid` ASC,`order`.`created` DESC, `order`.`id` DESC");
	$order_array = array();
	$num = 1;
	while($each_row = $get_order_query->fetch_assoc()) {
		$order = array();
		$order["id"] = $each_row["id"];
		$order["order_number"] = $num;
		$order["status"] = $each_row["name"];
		$order["netcost"] = $each_row["netcost"];
		$order["created"] = $each_row["created"];
		$num = $num+1;
		array_push($order_array,$order);
	}
	$result = json_encode($order_array);
	echo $result;
}
?>